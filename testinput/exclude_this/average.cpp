#include "average.h"

CAverage::CAverage()
{}

CAverage::~CAverage()
{}

void CAverage::Add(float Value)
{
  m_Values.push_back(Value);
}

float CAverage::Avg() const
{
  if (m_Values.empty())
    return 0.0f;

  double total = 0.0;
  for (std::vector<float>::const_iterator it=m_Values.begin();
       it != m_Values.end();
       ++it)
  {
    total += *it;
  }

  return static_cast<float>(total/m_Values.size());
}

void CAverage::Clear()
{
  m_Values.clear();
}

/*
---
TESTSUITE : exclude_this 
TYPE   : COLUMNBASED 
LINK : [ average.cpp ]
INCLUDE: [ average.h ]
STUBS  : 
PRE    : |
  CAverage A;
POST   : 
CODE   : [
  [
   [ 'TABLE', ~, 'EXPECT_EQ(0==$1)'],
   [ 'fail' , ~, '1'               ],
  ]
]
*/

