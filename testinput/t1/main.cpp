#include "average.h"
#include <iostream>

int main()
{
  CAverage a = CAverage();

  a.Add(10.0);
  a.Add(6.0);

  std::cout << "The Average Is..." << a.Avg() << std::endl;

  return 0;
}
