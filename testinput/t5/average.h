#ifndef __AVERAGE__H__
#define __AVERAGE__H__

#include <vector>
#include "logger.h"

class CAverage
{
  public:
    CAverage();
   ~CAverage();

    void Add(float value);
    float Avg() const; 
    void Clear();

  private:
    std::vector<float> m_Values;
    Logger m_Logger;
};

/*
---
TESTSUITE : t5_if_you_see_this_test_its_an_error
TYPE   : COLUMNBASED
LINK : [ average.cpp, logger.cpp ]
INCLUDE: [ average.h, logger.h ]
STUBS  : 
PRE    : |
  CAverage A;
POST   : 
CODE   : [
  [
   [ 'TABLE1'   , 'A.Add($1)' , ~, 'EXPECT_EQ(A.Avg(), $1)'],
   [ 'empty'    , ''          , ~, '0.0f'                  ],
   [ 'onevalue' , '3.14f'     , ~, '3.14f'                 ],
   [ 'twovalues', '2.0f,,4.0f', ~, '3.0f'                  ],
  ],
  [
   [ 'TABLE2'   , 'A.Clear()', 'A.Add($1)' , ~, 'EXPECT_EQ(A.Avg(), $1)'],
   [ 'empty'    , '.',         ''          , ~, '0.0f'                  ],
   [ 'onevalue' , '.',         '3.14f'     , ~, '3.14f'                 ],
   [ 'twovalues', '.',         '2.0f,,4.0f', ~, '3.0f'                  ]
  ],
]
*/



#endif
