#include "average.h"
#include "logger.h"

CAverage::CAverage()
{}

CAverage::~CAverage()
{}

void CAverage::Add(float Value)
{
  m_Values.push_back(Value);
}

float CAverage::Avg() const
{
  if (m_Values.empty())
  {
    m_Logger.Log("Warning: Avg of an empty sequence");
    return 0.0f;
  }

  double total = 0.0;
  for (std::vector<float>::const_iterator it=m_Values.begin();
       it != m_Values.end();
       ++it)
  {
    total += *it;
  }

  return static_cast<float>(total/m_Values.size());
}

void CAverage::Clear()
{
  m_Values.clear();
}

/*
@
\section{test embedded LaTeX}
#
---
TESTSUITE : t5_nostubs 
TYPE   : COLUMNBASED
LINK : [ average.cpp, logger.cpp ]
INCLUDE: [ average.h, logger.h ]
@
Whatever I type here should end up as documentation.
#
STUBS  : 
PRE    : |
  CAverage A;
POST   : 
CODE   : [
  [
   [ 'TABLE1'   , 'A.Add($1)' , ~, 'EXPECT_EQ(A.Avg(), $1)'],
   [ 'empty'    , ''          , ~, '0.0f'                  ],
   [ 'onevalue' , '3.14f'     , ~, '3.14f'                 ],
   [ 'twovalues', '2.0f,,4.0f', ~, '3.0f'                  ],
  ],
  [
   [ 'TABLE2'   , 'A.Clear()', 'A.Add($1)' , ~, 'EXPECT_EQ(A.Avg(), $1)'],
   [ 'empty'    , '.',         ''          , ~, '0.0f'                  ],
   [ 'onevalue' , '.',         '3.14f'     , ~, '3.14f'                 ],
   [ 'twovalues', '.',         '2.0f,,4.0f', ~, '3.0f'                  ]
  ],
]
*/

/*
---
TESTSUITE : t5_stubs
TYPE   : COLUMNBASED
LINK : [ average.cpp ]
INCLUDE: [ average.h ]
STUBS  : |
  void Logger::Log(const std::string &LogString) const {}
PRE    : |
  CAverage A;
POST   : 
CODE   : [
  [
   [ 'TABLE1'   , 'A.Add($1)' , ~, 'EXPECT_EQ(A.Avg(), $1)'],
   [ 'empty'    , ''          , ~, '0.0f'                  ],
   [ 'onevalue' , '3.14f'     , ~, '3.14f'                 ],
   [ 'twovalues', '2.0f,,4.0f', ~, '3.0f'                  ],
  ],
  [
   [ 'TABLE2'   , 'A.Clear()', 'A.Add($1)' , ~, 'EXPECT_EQ(A.Avg(), $1)'],
   [ 'empty'    , '.',         ''          , ~, '0.0f'                  ],
   [ 'onevalue' , '.',         '3.14f'     , ~, '3.14f'                 ],
   [ 'twovalues', '.',         '2.0f,,4.0f', ~, '3.0f'                  ]
  ],
]
*/


