#ifndef __LOGGER__H__
#define __LOGGER__H__

#include <string>

class Logger
{
  public:
    void Log(const std::string &LogString) const;
};

#endif
