#ifndef __AVERAGE__H__
#define __AVERAGE__H__

#include <vector>

class CAverage
{
  public:
    CAverage();
   ~CAverage();

    void Add(float value);
    float Avg() const; 

  private:
    std::vector<float> m_Values;
};

#endif
