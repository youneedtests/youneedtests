#include "average.h"

CAverage::CAverage()
{}

CAverage::~CAverage()
{}

void CAverage::Add(float Value)
{
  m_Values.push_back(Value);
}

float CAverage::Avg() const
{
  double total = 0.0;
  for (std::vector<float>::const_iterator it=m_Values.begin();
       it != m_Values.end();
       ++it)
  {
    total += *it;
  }

  return static_cast<float>(total/m_Values.size());
}

/*
---
TESTSUITE : t3 
TYPE   : RAW
LINK : [ average.cpp ]
INCLUDE: [ average.h ]
STUBS  : 
CODE   : |
  CAverage A;
  A.Add(4.0f);
  A.Add(8.0f);
  EXPECT_EQ(A.Avg(), 6.0f);
*/

