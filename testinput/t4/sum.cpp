#include "sum.h"

CSum::CSum()
{}

CSum::~CSum()
{}

void CSum::Add(float Value)
{
  m_Values.push_back(Value);
}

float CSum::Sum() const
{
  float total = 0.0f;
  for (std::vector<float>::const_iterator it=m_Values.begin();
       it != m_Values.end();
       ++it)
  {
    total += *it;
  }

  return total;
}

void CSum::Clear()
{
  m_Values.clear();
}

/*
---
TESTSUITE : t4Sum 
TYPE   : COLUMNBASED
LINK : [ sum.cpp ]
INCLUDE: [ sum.h ]
STUBS  : 
PRE    : |
  CSum A;
POST   : 
CODE   : [
  [
   [ 'TABLE1'   , 'A.Add($1)' , ~, 'EXPECT_EQ(A.Sum(), $1)'],
   [ 'empty'    , ''          , ~, '0.0f'                  ],
   [ 'onevalue' , '3.14f'     , ~, '3.14f'                 ],
   [ 'twovalues', '2.0f,,4.0f', ~, '6.0f'                  ],
  ],
  [
   [ 'TABLE2'   , 'A.Clear()', 'A.Add($1)' , ~, 'EXPECT_EQ(A.Sum(), $1)'],
   [ 'empty'    , '.',         ''          , ~, '0.0f'                  ],
   [ 'onevalue' , '.',         '3.14f'     , ~, '3.14f'                 ],
   [ 'twovalues', '.',         '2.0f,,4.0f', ~, '6.0f'                  ]
  ],
]
*/

