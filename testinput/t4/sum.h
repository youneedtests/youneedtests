#ifndef __SUM__H___
#define __SUM__H__

#include <vector>

class CSum
{
  public:
    CSum();
   ~CSum();

    void Add(float value);
    float Sum() const; 
    void Clear();

  private:
    std::vector<float> m_Values;
};

#endif
