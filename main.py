# This file is part of YouNeedTests.
#
#    YouNeedTests is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    YouNeedTests is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with YouNeedTests.  If not, see <http://www.gnu.org/licenses/>.


if __name__ == "__main__":
  import argparse
  parser = argparse.ArgumentParser(description='YouNeedTests 0.0.1 - extract tests')
  parser.add_argument('--filename', metavar='filename', type=str,
                         help='filename in which the testsuite(s) live')
  parser.add_argument('--srcfolder', metavar='srcfolder', type=str,
                         help='folder to scan for files with testsuites')
  parser.add_argument('--recursive', dest="recursive", action="store_true", default=False,
                         help='recursively scan srcfolder for testsuites (this overrides --filename)')
  parser.add_argument('--file-extension', metavar='file_extension', default=[],
                         action='append',
                         help='only file extensions registered using --file-extension will be looked at. If --file-extension is not used, all files are allowed')
  parser.add_argument('--exclude-folder', dest='excludefolder', default=[],
                         action='append',
                         help='if using --recursive, do not descend into this folder (you can use this argument multiple times)')
  parser.add_argument('--testsuite', metavar='testsuite', type=str,
                         help="testsuite (don't use this option if you want extract *all* testsuites in the file)")
  parser.add_argument('--outputfolder', metavar='outputfolder', type=str,
                         help='outputfolder (the source code for the tests will be generated there)')
  parser.add_argument('--language', metavar='language', type=str,
                         help='language (valid ones: cpp)')
  parser.add_argument('--generator', metavar='generator', type=str,
                         help='generator (valid ones: googletest)')
  parser.add_argument('--generator-include-dir', metavar='generator_include_dir', type=str,default="",
                         help='generator additional include path')

  options = parser.parse_args()
  
  import TestExtractor
  t = TestExtractor.TestExtractor()
  t.extract(options)

