import re
import os.path 
import sys

def get_install_path():
    the_path = os.path.dirname(sys.argv[0])
    if not the_path:
        the_path = ""
    return the_path

class SemilitProcessor(object):
    def __init__(self, options):
        self.new_docfile_matcher = re.compile("^@@@ (?P<filename>.*)")
        self.new_codefile_matcher = re.compile("^### (?P<filename>.*)")
        self.new_docsection_matcher = re.compile("^@\w*$")
        self.new_codesection_matcher = re.compile("^#\w*$")
        self.options = options
        self.reset()

    def force_codefile(self, name):
        self.codefile = name
        self.codefile, self.codef, error = self.opencodefile(self.codefile, self.codef)
        if error:
            sys.exit(-1)

    def force_docfile(self, name):
        self.docfile = name
        self.docfile, self.docf, error = self.opendocfile(self.docfile, self.docf)
        if error:
            sys.exit(-1)

    def writeline(self, fileobject, line):
        try:
            fileobject.write(line)
            fileobject.write("\n")
        except IOError:
            print ("Error while trying to write line %s to file." % line)

    def emit(self, what, fileobject):
        try:
            fo = open(os.path.join(get_install_path(),
                      "backend",
                      self.options.backend,
                      "%s.template" % what), "r")
            for line in fo:
                fileobject.write(line)
        except IOError:
            print ("Error while trying to copy template %s into final file." % what)

    def emit_start_of_doc_file(self, fileobject):
        self.emit("start_of_doc_file", fileobject)

    def emit_end_of_doc_file(self, fileobject):
        self.emit("end_of_doc_file", fileobject)

    def emit_start_of_code_file(self, fileobject):
        self.emit("start_of_code_file", fileobject)

    def emit_end_of_code_file(self, fileobject):
        self.emit("end_of_code_file", fileobject)

    def emit_start_of_code_in_doc_section(self, fileobject):
        self.emit("start_of_code_in_doc_section", fileobject)

    def emit_end_of_code_in_doc_section(self, fileobject):
        self.emit("end_of_code_in_doc_section", fileobject)

    def openfile(self, filename, fileobject, set_of_encountered_files, writeheader_fun):
        NO_ERROR = False
        ERROR = True
        try:
            fileobject.close()
        except IOError:
            pass
        except AttributeError:
            pass
        try:
            absfilename = os.path.abspath(filename.strip())
            if not absfilename in set_of_encountered_files:
                newfileobject = open(absfilename, "w")
                writeheader_fun(newfileobject)
                set_of_encountered_files.add(absfilename)
            else:
                newfileobject = open(absfilename, "a")
            return absfilename, newfileobject, NO_ERROR
        except IOError:
            print ("Error opening doc file %s for writing. Continuing in previous mode. Keeping fingers crossed." % absfilename)
            return filename, fileobject, ERROR

    def opendocfile(self, filename, fileobject):
        return self.openfile(filename, fileobject, self.doc_files_encountered, self.emit_start_of_doc_file)

    def opencodefile(self, filename, fileobject):
        return self.openfile(filename, fileobject, self.code_files_encountered, self.emit_start_of_code_file)

    def reset(self):
        self.docfile = "sandbox/extracted_doc.tex"
        self.docf = None
        self.codefile = "sandbox/extracted_code.py"
        self.codef = None
        self.mode = "CODE"
        self.doc_files_encountered = set([])
        self.code_files_encountered = set([])
        self.code_in_doc_active = {}
        self.force_docfile(self.docfile)
        self.force_codefile(self.codefile)

    def code_in_doc_ongoing(self, filename):
        if not filename in self.code_in_doc_active:
            return False
        return self.code_in_doc_active[filename]

    def finish_all_files(self):
        for file in self.code_files_encountered:
            f, fobj, error = self.opencodefile(file, self.codef)
            if not error:
                try:
                    self.emit_end_of_code_file(fobj)
                    fobj.close()
                except IOError:
                    print ("Error while trying to finish code file %s" % f)
                    fobj.close()

        for file in self.doc_files_encountered:
            f, fobj, error = self.opendocfile(file, self.docf)
            if not error:
                if self.code_in_doc_ongoing(f):
                    try:
                        self.emit_end_of_code_in_doc_section(fobj)
                    except IOError:
                        print ("Error while trying to finish doc file %s" % f)
                        fobj.close()
                try:
                    self.emit_end_of_doc_file(fobj)
                    fobj.close()
                except IOError:
                    print ("Error while trying to finish doc file %s" % f)
                    fobj.close()

    def run(self, src):
        for line in src.splitlines():
            docfilematch = self.new_docfile_matcher.match(line)
            codefilematch = self.new_codefile_matcher.match(line)
            docsectionmatch = self.new_docsection_matcher.match(line)
            codesectionmatch = self.new_codesection_matcher.match(line)
            if docfilematch:
                docfile = docfilematch.group("filename")
                docfilestrip = docfile.strip()
                if docfilestrip:
                    self.docfile, self.docf, error = self.opendocfile(docfilestrip, self.docf)
                    if not error:
                        self.mode = "DOCUMENTATION"
                        if self.code_in_doc_ongoing(self.docfile):
                           self.emit_end_of_code_in_doc_section(self.docf)
                           self.code_in_doc_active[self.docfile] = False

            elif codefilematch:
                codefile = codefilematch.group("filename")
                codefilestrip = codefile.strip()
                if codefilestrip:
                     self.codefile, self.codef, error = self.opencodefile(codefilestrip, self.codef)
                     if not error:
                        self.mode = "CODE"

            elif docsectionmatch:
                self.mode = "DOCUMENTATION"
                if self.code_in_doc_ongoing(self.docfile):
                   self.emit_end_of_code_in_doc_section(self.docf)
                   self.code_in_doc_active[self.docfile] = False

            elif codesectionmatch:
                self.mode = "CODE"

            else:
                if self.mode == "DOCUMENTATION":
                    try:
                        self.writeline(self.docf, line)
                    except IOError:
                        print ("Error writing content to documentation file %s." % self.docfile)
                elif self.mode == "CODE":
                    try:
                        self.writeline(self.codef, line)
                        if self.options.add_code_to_documentation:
                            if not self.code_in_doc_ongoing(self.docfile):
                                self.emit_start_of_code_in_doc_section(self.docf)
                                self.code_in_doc_active[self.docfile] = True
                            self.writeline(self.docf, line)
                    except IOError:
                        print ("Error writing content to code file %s." % self.codefile)

        self.finish_all_files()

if __name__ == "__main__":
    test_spec = r"""@@@ documentation.tex
\section{Intro}
We start in documentation mode

### file1.py
def somecode(with_a_parameter):
    blahblah()

@@@ another_documentation_file.tex
some more documentation can be added

#
def somemorecode(with_another_parameter):
    bleeblee()

@@@ documentation.tex
\section{Extro}
and hopla we're back in documentation mode!
Isn't that nice?

And now for some embedded graphics:
\begin{figure}
\centering
\begin{asy}
\input "rectangle.asy"
rectangle("R1",0,0,45,10,20);
rectangle("R2",10,10,33,10,20);
\end{asy}
\caption{Embedded Asymptote figures are easy!}
\label{fig:embedded}
\end{figure}

@@@ another_documentation_file.tex
Bye now!

#
def andsomecodetoend():
    pass
"""
    class Options(object):
        pass

    o = Options()
    o.backend = "latex-asymptote-python"
    o.add_code_to_documentation = True

    p = SemilitProcessor(o)
    p.run(test_spec)

