# This file is part of YouNeedTests.
#
#    YouNeedTests is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    YouNeedTests is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with YouNeedTests.  If not, see <http://www.gnu.org/licenses/>.


def ensure_dir(d):
    """
    create all folders as implied by string d
    (e.g. if the file system contains an empty folder /data
     and d == /data/username/blahblah, and if the function
     succeeds, after finishing there should be a folder
     /data/username/blahblah
    """
    import os
    import os.path
    if not os.path.exists(d):
        os.makedirs(d)

def template_path():
  import os.path
  import sys
  thePath = os.path.abspath(os.path.dirname(sys.argv[0]))
  return os.path.join(thePath)

class TestExtractor(object):
  def extract(self, options):
    """
    this method loads a suitable CommentTool and CodeGenerator,
    and uses it to generate the code for the tests embedded in the 
    options.inputfile
    """
    import yaml
    # first dynamically import the correct backend classes
    try:
      classname_commentdetector = "backend.%s.CommentTool" % options.language
      classname_codegenerator = "backend.%s.%s.CodeGenerator" % (options.language, options.generator)
      classname_postbuildaction = "backend.%s.%s.PostBuildAction" % (options.language, options.generator)
      classname_extraincludes = "backend.%s.%s.ExtraIncludes" % (options.language, options.generator)
      comment_module = __import__(classname_commentdetector, fromlist=['CommentTool'])
      CommentTool = getattr(comment_module, 'CommentTool')
      codegen_module = __import__(classname_codegenerator, fromlist=['CodeGenerator'])
      CodeGenerator = getattr(codegen_module, 'CodeGenerator')
      postbuild_module = __import__(classname_postbuildaction, fromlist=['PostBuildAction'])
      PostBuildAction = getattr(postbuild_module, 'PostBuildAction')
      extraincludes_module = __import__(classname_extraincludes, fromlist=['ExtraIncludes'])
      ExtraIncludes = getattr(extraincludes_module, 'ExtraIncludes')
    except Exception as e:
      print("Error: couldn't load one of the requested languages/generators.")
      print(repr(e))
      return

    list_of_files = []
    if options.filename:
        list_of_files = [options.filename]
    import FileCollector    
    if options.recursive:
      list_of_files = FileCollector.find_all_files_recursively(options.srcfolder, "*.*", options.excludefolder)

    if options.file_extension:
      list_of_files_filtered = []
      for ext in options.file_extension:
        filtered = [ f for f in list_of_files if f.endswith(ext) ]
        list_of_files_filtered.extend(filtered)
      list_of_files = list_of_files_filtered


    projects=[]

    # bunch of heuristics to speed up parsing
    import re
    TESTSUITE_RECOGNIZER = re.compile("TESTSUITE")
    TESTSUITE_RECOGNIZER2 = None
    if options.testsuite:
      TESTSUITE_RECOGNIZER2 = re.compile(options.testsuite) 
    SEMILIT_RECOGNIZER = re.compile("@")

    for filename in list_of_files:
      print (filename)
      import os.path
      options.path_prefix = os.path.dirname(FileCollector.subtract_path(options.srcfolder, filename))

      # read the input file
      txt = ""
      try:
        #print(filename)
        with open(filename) as txtf:
          txt = txtf.read()
      except IOError as e:
        print("Could not open file %s for reading." % filename)
        print(repr(e))
        return 
      except UnicodeDecodeError as e:
        pass

      # extract all comments
      e = CommentTool()
      comments = e.comments(txt)
      codegen = CodeGenerator()


      # for each comment, extract the embedded tests (if any)
      for c in comments:
        # heuristic to check if we should bother parsing this comment further
        if TESTSUITE_RECOGNIZER.search(c) and ((not TESTSUITE_RECOGNIZER2) or TESTSUITE_RECOGNIZER2.search(c)):
            # semilit is quite slow -> only run it if there's a chance that something usable will be produced.
            if SEMILIT_RECOGNIZER.search(c):

                if os.path.isfile("sandbox/extracted_code.py"):
                    os.remove("sandbox/extracted_code.py")
                if os.path.isfile("sandbox/extracted_doc.tex"):
                    os.remove("sandbox/extracted_doc.tex")
                
                from SemiLit import SemilitProcessor
                class Options(object):
                    pass
    
                o = Options()
                o.backend = "latex-asymptote-python"
                o.add_code_to_documentation = True
                semilit = SemilitProcessor(o)
                semilit.run(c)
    
                try:
                   fl = open("sandbox/extracted_code.py","r")
                   c = fl.read()
                   #print ("SEMILIT RESULTS:\n", c)
                   fl.close()
                except IOError:
                   pass
    
        else:
          continue

        # try to convert the comment to code; in case something goes wrong, assume
        # that the comment didn't fullfill all requirements to be interpreted as a test specification       
        try:
          try:
              #print("LOADING")
              #print(c.strip()[2:-2])
              testspec = yaml.load(c.strip()[2:-2])
              #print("LOAD FINISHED")
          except yaml.YAMLError as exc:
                print ("Error in configuration file:", exc)
    
          # a test suite is relevant if we either didn't specify a testsuite name on the command line,
          # or if the testsuitename matches the name of the testsuite specified on the command line
          if not options.testsuite or \
             (options.testsuite and testspec['TESTSUITE'] == options.testsuite):
            # otherwise, create a suitable output path and generate the code there
            try:
              code = codegen.generate(testspec, options)
              name = codegen.testsuitename
              import os.path
              full_outputpath= os.path.join(options.outputfolder, name)
              ensure_dir(full_outputpath)
              buildscript = codegen.generate_buildscript(testspec, 
                      options, 
                      os.path.abspath(full_outputpath), 
                      ExtraIncludes.extra_includefolders)
              full_name = os.path.join(full_outputpath,name + "_unittests.cpp")
              buildscript_name = os.path.join(full_outputpath, "CMakeLists.txt")
              import os
              import re
              bytescode = bytes(code, 'UTF-8')
              bytescode2= re.sub(b"\r?\n", bytes(os.linesep,'latin-1'), bytescode)                
              if not options.outputfolder:
                print(bytescode2)
                print(buildscript)
              else:
                with open(full_name, "wb") as f:
                  f.write(bytescode2)    
                with open(buildscript_name, "w") as f:
                  f.write(buildscript)
              # track which projects have been created. we need this
              # when we generate the build script that compiles all tests
              projects.append(os.path.basename(full_outputpath))
            except IOError as e:
              print("Error! Couldn't open file %s for writing." % full_name)
              print(repr(e))
        except Exception as e:
          print("Error! %s" % repr(e))
          continue
 
    # now write a CMakeLists.txt that encompasses all generated projects
    from mako.template import Template
    import os.path
    mytemplate = Template(filename=os.path.join(template_path(), "backend", options.language, options.generator, "templates", "template.cmakelists.mako"))
    rendered_template = mytemplate.render(projects=projects)
    if not options.outputfolder:
      print(rendered_template)
    else:
      try:
        full_name = os.path.join(options.outputfolder,"CMakeLists.txt") 
        with open(full_name,"w") as f:
          # print(projects)
          f.write(rendered_template)
      except IOError as e:
        print("Error! Couldn't open file %s for writing." % full_name)

    pba = PostBuildAction(template_path(), options.outputfolder)
    pba.run()

