# This file is part of YouNeedTests.
#
#    YouNeedTests is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    YouNeedTests is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with YouNeedTests.  If not, see <http://www.gnu.org/licenses/>.

import os

class Test(object):
  """
  class to hold a single testdefinition in table based test spec
  (using information from one row of the table in a columnbased table;
   information from all rows of the table in a rowbased table)
  """
  def __init__(self, testname="", statements={ True : [], False : [] }):
    self.testname = testname
    self.statements = statements
  
class Table(object):
  """
  base class to convert a table spec to code
  """
  def __init__(self, prespec, postspec, tablespec):
    self.prespec = prespec
    self.postspec = postspec
    self.tablespec = tablespec
    self.statements = { True : [], False : [] }
    self.name = tablespec[0][0] 
    self.testname = ""
    self.tests = []

    self.render()

  def render_cell(self, codetemplate, cellcontents, is_assertion):
    """
    generate code from codetemplate, using values in cellcontents
    """
    # clean up template
    codetemplate = codetemplate.strip()
    reverse_enumerate = lambda l : zip(range(len(l)-1,-1,-1),reversed(l))
    # unroll over ,,
    splitted_contents = cellcontents.split(",,")
    for values in splitted_contents:
      workingtemplate = codetemplate[:]
      # distinguish between $1, $2, $3, ...
      splitted_value = values.split(",")
      # only render non-empty cells
      if values:
        # render $10 before $1 to avoid wrong substitutions
        for idx, v in reverse_enumerate(splitted_value):
          if v:
            workingtemplate = workingtemplate.replace("$%d" % (idx+1), v)
        # add a semicolon if it was not present yet
        if workingtemplate.endswith(";"):
          eol = ""
        else:
          eol = ";"
        # add it to a list of statements (or assertions)
        self.statements[is_assertion].append(workingtemplate+eol)

  def render_line(self, tableheader, tableline):
    """
    tables consist of lines; first line is a table header;
    remaining lines define scenarios from which to generate code
    this function renders a table line to code, by using the table header
    as code template
    """
    is_assertion = False
    first_col = True
    for codetemplate, cellcontents in zip(tableheader, tableline):
      # switch from code mode to assertion mode upon encountering a None column
      if first_col:
        self.testname = cellcontents
        first_col = False
      else:
        if codetemplate is None:
          is_assertion = True
          continue
        # render each cell
        self.render_cell(codetemplate, cellcontents, is_assertion)

    return Test(self.testname, self.statements)

  def render(self):
    """
    renders each line in the table; first line contains code templates;
    subsequent lines contain the test definitions
    """
    self.tests = []
    for line in self.tablespec[1:]:
      # reset internal state
      self.statements = { True : [], False : [] }
      self.testname = ""
      # render to code
      self.tests.append(self.render_line(self.tablespec[0], line))


class Generator(object):
  """
  base class from which to derive all kinds of code generators
  """
  def template_path(self):
    import os.path
    import sys
    thePath = os.path.dirname(sys.argv[0])
    return os.path.join(thePath, "backend/cpp/googletest")

class TableBasedGenerator(Generator):
  """
  generator for table based test specifications
  this is a base class from which we derive column based and
  row based generators
  """
  def generate(self, testspec, options):
    """
    convert a testspec into code
    some of the required details are provided in derived classes:
     * name of mako template in variable template_name
     * name of testsuite type in variable TYPE
    """
    from mako.template import Template
    mytemplate = Template(filename=os.path.join(self.template_path(), self.template_name))
    tables = []
    for t in testspec['CODE']: # for each table
      table = Table(testspec['PRE'], testspec['POST'], t)
      tables.append(table)
    return mytemplate.render(
        includes = testspec['INCLUDE'],
        testsuite = testspec['TESTSUITE'],
        stubs = testspec['STUBS'],
        tables = tables
        ) 

class ColumnBasedGenerator(TableBasedGenerator):
  """
  class that provides all the details to generate 
  column based tests
  """
  TYPE = 'COLUMNBASED'
  def __init__(self):
    self.template_name = 'template.columnbased.mako'

class RowBasedGenerator(TableBasedGenerator):
  """
  class that provides all the details to generate 
  column based tests
  """
  TYPE = 'ROWBASED'
  def __init__(self):
    self.template_name = 'template.rowbased.mako'

class RawGenerator(Generator):
  """
  class to generate code from a RAW-type testsuite
  """
  TYPE = 'RAW'

  def generate(self, testspec, options):
    """
    generate code from a RAW-type testsuite
    """
    from mako.template import Template
    mytemplate = Template(filename=os.path.join(self.template_path(), 'template.raw.mako'))
    return mytemplate.render(
        includes = testspec['INCLUDE'],
        testsuite = testspec['TESTSUITE'],
        stubs = testspec['STUBS'],
        code = testspec['CODE']
        )

class CodeGenerator(object):
  """
  class that generates code for any of the supported testsuite types
  most of the work is delegated to one of 
   * ColumnBasedGenerator
   * RowBasedGenerator
   * RawGenerator
  """
  def __init__(self):
    self.testsuitename = ""

  def generate(self, testspec, options):
    """
    generate code for a testsuite if the type is supported
    """
    self.testsuitename = testspec['TESTSUITE']
    type_to_class = { ColumnBasedGenerator.TYPE : ColumnBasedGenerator, 
                      RowBasedGenerator.TYPE    : RowBasedGenerator,
                      RawGenerator.TYPE         : RawGenerator }
    if testspec['TYPE'] in type_to_class:
      gen = type_to_class[testspec['TYPE']]()
      return gen.generate(testspec, options)
    else:
      print("Error! Currently selected backend does not support testsuites of type %s" % testspec['TYPE'])
      return

  def generate_buildscript(self, testspec, options, full_outputpath, extra_includes):
    """
    generate a build script (CMake based) to build the tests
    """
    testsuite = testspec['TESTSUITE']
    from mako.template import Template
    mytemplate = Template(filename=os.path.join(Generator().template_path(), 'template.cmakelist.mako'))
    all_includes = options.generator_include_dir
    code_deps = ''
    include_dirs = ''
    binlibs = ''
    symbols = ''
    if 'INCLUDE' in testspec:
      if testspec['INCLUDE']:
          for d in testspec['INCLUDE']:
            all_includes += " " + os.path.join(options.srcfolder, options.path_prefix, os.path.dirname(d))

    for d in extra_includes:
      all_includes += " " + os.path.join(full_outputpath, d)

    if 'LINK' in testspec:
      if testspec['LINK']:
          for d in testspec['LINK']:
            code_deps += " " + os.path.join(options.srcfolder, options.path_prefix, d)

    if 'INCLUDEDIR' in testspec:
      if testspec['INCLUDEDIR']:
          for d in testspec['INCLUDEDIR']:
            include_dirs += " " + d

    if 'BINLIB' in testspec:
      if testspec['BINLIB']:
          for d in testspec['BINLIB']:
            binlibs += " " + d

    if 'SYMBOLS' in testspec:
      if testspec['SYMBOLS']:
          for d in testspec['SYMBOLS']:
            symbols += " -D" + d

    return mytemplate.render(
        testsuite = testsuite,
        code_deps = code_deps,
        includes = all_includes,
        include_dirs = include_dirs,
        binlibs = binlibs,
        symbols = symbols)


