cmake_minimum_required (VERSION 2.8)
project (ALL_TESTS) 
%for project in projects:
add_subdirectory(${project})
%endfor
ENABLE_TESTING()
