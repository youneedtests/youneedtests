class ExtraIncludes(object):
    """
    extra folders needed to compile with this backend
    path specified relatively w.r.t. the generated test folder
    """
    extra_includefolders = ["../googletest/include"]


