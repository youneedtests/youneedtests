<%!
    def slashcorrect(text):
        return text.replace("\\","/")
%>
cmake_minimum_required (VERSION 2.8)
project (${testsuite})
add_executable(${testsuite} ${code_deps | slashcorrect} ${testsuite}_unittests.cpp)
%if symbols: 
add_definitions(${symbols})
%endif
add_definitions(-D_AFXDLL=1)
include_directories(${includes | slashcorrect} ${include_dirs | slashcorrect} )
target_link_libraries (${testsuite} ${binlibs} gtest gtest_main)
find_package(Threads REQUIRED)
target_link_libraries(${testsuite} <%text>${CMAKE_THREAD_LIBS_INIT}</%text>)

ENABLE_TESTING()
ADD_TEST(${testsuite}_test <%text>${CMAKE_CURRENT_BINARY_DIR}</%text>/${testsuite})

