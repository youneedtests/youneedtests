class PostBuildAction(object):
  def __init__(self, installationfolder, outputfolder):
    self.installationfolder = installationfolder
    self.outputfolder = outputfolder

  def run(self):
    import shutil
    import os.path
    try:
      shutil.copytree(os.path.join(self.installationfolder, "backend", "cpp", "googletest", "googletest"),
                      os.path.join(self.outputfolder,"googletest"))
    except Exception as e:
      print("Errors! %s" % repr(e))

    try:
      with open(os.path.join(self.outputfolder,"CMakeLists.txt"), "a") as f:
        f.write("add_subdirectory(googletest)")
    except Exception as e:
      print("Errors! %s" % repr(e))

    
