# This file is part of YouNeedTests.
#
#    YouNeedTests is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    YouNeedTests is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with YouNeedTests.  If not, see <http://www.gnu.org/licenses/>.


import re

class CommentTool(object):
    def __init__(self):
        def q(c):
            """Returns a regular expression that matches a region
               delimited by c, inside which c may be escaped with
               a backslash.
            """
            return r"%s(\\.|[^%s])*%s" % (c,c,c)

        self.singleQuotedStringPattern = q('"')
        self.doubleQuotedStringPattern = q("'")
        self.cCommentPattern = r"/\*.*?\*/"
        self.cppCommentPattern = r"//[^\n]*"

        self.rx = re.compile("|".join([self.singleQuotedStringPattern,
                                       self.doubleQuotedStringPattern,
                                       self.cCommentPattern,
                                       self.cppCommentPattern]),
                             re.DOTALL)

    def replace(self, x):
        y = x.group(0)
        if y.startswith("/"):
            return ""
        return y

    def strip_comments(self, fromString):
        return self.rx.sub(self.replace, fromString)

    def comments_iterator(self, fromString):
        return self.rx.finditer(fromString)

    def comments(self, fromString):
        c = self.rx.finditer(fromString)
        return [ c.group(0) for c in self.comments_iterator(fromString) if c.group(0).startswith("/") ]


if __name__=="__main__":
    code = r"""
#include "Cad_int.h"
#include <io.h>
#include "..\env\ParamFile.h"
//#include "DontFindMe.h"

/************************************************************************************************************/

            /*
#include "orme.h"  // should work correctly!
*/

#include "..\env\linkedcontrol.h"

/* TEXT CONSTANT FOR ENUMERATION */
const WORD TxtCompType[]        = { TXT_CMP_MODL_FOURSIDED, TXT_CMP_MODL_TWOSIDED, TXT_CMP_MODL_IRREGULAR, TXT_CMP_MODL_BGA, TXT_CMP_MODL_LEADLESS, TXT_CMP_MODL_PGA, TXT_CMP_MODL_LGA, NULL };
""" 
    code2 = r"""
// constants for Enumeration parameters (Write/Read model files )
#include "..\mmi\mmi2.h"
    """

    stripper = CommentTool()
    print("BEFORE:")
    print(code)
    print("Comments: ")
    for c in stripper.comments(code):
        print("")
        print("")
        print("Comment...")
        print(c)
        print("EOC")
        print("")
        print("")


    print("BEFORE:")
    print(code)
    print("AFTER:")
    print(stripper.strip_comments(code))

    print("BEFORE:")
    print(code2)
    print("AFTER:")
    print(stripper.strip_comments(code2))
    
    

