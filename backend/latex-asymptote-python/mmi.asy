import math;
size(0,5cm);
picture mmi_rectangle( picture pic=currentpicture, string cornername="C", real cx, real cy, real angle, real width, real height) {
    pair c1 = shift(cx, cy)*rotate(angle)*(-0.5*width, -0.5*height);
    pair c2 = shift(cx, cy)*rotate(angle)*(-0.5*width,  0.5*height);
    pair c3 = shift(cx, cy)*rotate(angle)*( 0.5*width,  0.5*height);
    pair c4 = shift(cx, cy)*rotate(angle)*( 0.5*width, -0.5*height);
//    write(c1);
//    write(c2);
//    write(c3);
//    write(c4);
    draw(pic, c1--c2--c3--c4--cycle);
    label(pic, "$"+cornername+"_1$", c1, blue);
    label(pic, "$"+cornername+"_2$", c2, blue);
    label(pic, "$"+cornername+"_3$", c3, blue);
    label(pic, "$"+cornername+"_4$", c4, blue);
    return pic;
}

picture mmi_linesegment( picture pic=currentpicture, string endpointname="S", real p1x, real p1y, real p2x, real p2y) {
   draw(pic, (p1x,p1y) -- (p2x,p2y));
   dot(pic, "$"+endpointname+"_1$", (p1x,p1y),NW,blue);
   dot(pic, "$"+endpointname+"_2$", (p2x,p2y),SE,blue);
   return pic;
}

picture mmi_line(picture pic=currentpicture, string endpointname="S", real p1x, real p1y, real p2x, real p2y)
{
   drawline(pic, (p1x,p1y), (p2x, p2y));
   mmi_linesegment(pic, endpointname, p1x, p1y, p2x, p2y);
   return pic;
}


picture mmi_point( picture pic=currentpicture, string endpointname="P", real px, real py) {
   dot(pic, "$"+endpointname+"$", (px,py), N, blue);
   return pic;
}


