<%!
def list_of_html_from_list_of_cpp(list_of_cpp):
   print list_of_cpp
   list_of_html = [ (".".join(el.split(".")[:-1]))+".html" for el in list_of_cpp ]
   list_of_html.sort()
   return list_of_html
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml11-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Overview of YouNeedTest documentation</title>
</head>
<body>

<h1> The following files were generated during the last YouNeedTest execution: </h1>
<ul>
%for HTMLFILE in list_of_html_from_list_of_cpp(CPPFILES):
<li>
<a href="/metrilet/static/doctest/${HTMLFILE}"> ${HTMLFILE} </a>
</li>
%endfor
</ul>
</body>
</html>
