rm testoutput -rf
mkdir testoutput

python3.2 main.py --srcfolder testinput --recursive --outputfolder testoutput --exclude-folder testinput/exclude_this --file-extension .cpp --language cpp --generator googletest

cd testoutput
cmake .
make
make test

