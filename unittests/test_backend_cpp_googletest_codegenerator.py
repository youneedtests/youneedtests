import sys
sys.path.append("..")
from backend.cpp.googletest.CodeGenerator import Table,Test
import unittest

class TestGoogletestCodeGeneratorl(unittest.TestCase):
  """
  tests for the comment tool
  """
  def setUp(self):
    self.table = Table("pre();", "post();", [["dummy"]])

  def test_render_cell_single_placeholder_single_content(self):
    codetemplate = "print($1); "
    cellcontents = "1"
    self.table.render_cell(codetemplate, cellcontents, True)
    self.assertEqual(self.table.statements[True], ['print(1);'])

  def test_render_cell_single_placeholder_multiple_content(self):
    codetemplate = "print($1) "
    cellcontents = " 1,,2 " 
    self.table.render_cell(codetemplate, cellcontents, True)
    self.assertEqual(self.table.statements[True], ['print( 1);','print(2 );'])

  def test_render_cell_two_placeholders_single_content(self):
    codetemplate = "object->$1($2)"
    cellcontents = "Foo,45.0f"
    self.table.render_cell(codetemplate, cellcontents, True)
    self.assertEqual(self.table.statements[True], ['object->Foo(45.0f);'])

  def test_render_cell_two_placeholders_multiple_content(self):
    codetemplate = "call($1,$3,$2,$1)"
    cellcontents = "1,2,3,,4,5,6,,7,8,9"
    self.table.render_cell(codetemplate, cellcontents, True)
    self.assertEqual(self.table.statements[True], ['call(1,3,2,1);', 'call(4,6,5,4);', 'call(7,9,8,7);'])

  def test_render_empty_cell(self):
    codetemplate = "call($1)"
    cellcontents = ''
    self.table.render_cell(codetemplate, cellcontents, True)
    self.assertEqual(self.table.statements[True], [])

  def test_render_cell_without_placeholder(self):
    codetemplate = "A.clear()"
    cellcontents = "x"
    self.table.render_cell(codetemplate, cellcontents, True)
    self.assertEqual(self.table.statements[True], ["A.clear();"])

  def test_render_cell_with_more_than_9_placeholders(self):
    codetemplate = "$11,$1"
    cellcontents = "1,2,3,4,5,6,7,8,9,10,11"
    self.table.render_cell(codetemplate, cellcontents, True)
    self.assertEqual(self.table.statements[True], ["11,1;"])


  def test_render_line_code_and_assertion(self):
    header = [ "TABLENAME", "init($1,$2)", "prepare()", None, "assert(X==$2)" ]
    line  =  [ "testname" , "1,2,,4,5"   , "x"   , None, "true"          ] 
    expected_statements = {}
    expected_statements[False] = ["init(1,2);","init(4,5);","prepare();"]
    expected_statements[True]  = ["assert(X==$2);"]
    ExpectedResult = Test("testname", expected_statements)
    Result = self.table.render_line(header, line)
    self.assertEqual(Result.testname, ExpectedResult.testname)
    self.assertEqual(Result.statements, ExpectedResult.statements)

  def test_render_line_code_without_assertion(self):
    header = ["TABLENAME", "init($1, $2)", "prepare()"]
    line = ["testname"   , "1,2"         , ""         ]
    expected_statements = {}
    expected_statements[False] = ["init(1, 2);"]
    expected_statements[True]  = []
    ExpectedResult = Test("testname", expected_statements)
    Result = self.table.render_line(header, line)
    self.assertEqual(Result.testname, ExpectedResult.testname)
    self.assertEqual(Result.statements, ExpectedResult.statements)

  def test_render_line_only_assertions(self):
    header = ["TABLENAME", None, "assert(X==$1)", 'otherassert("BAR",$2)' ]
    line   = ["testname" , None, '"boo"'        , '45,"BAH"'              ]
    expected_statements = {}
    expected_statements[False] = []
    expected_statements[True] = ['assert(X=="boo");', 'otherassert("BAR","BAH");']
    ExpectedResult = Test("testname", expected_statements)
    Result = self.table.render_line(header,line)
    self.assertEqual(Result.testname, ExpectedResult.testname)
    self.assertEqual(Result.statements, ExpectedResult.statements)


if __name__ == "__main__":
    unittest.main()

