import sys
sys.path.append("..")
from backend.cpp.CommentTool import CommentTool
import unittest

class TestCppCommentTool(unittest.TestCase):
  """
  tests for the comment tool
  """
  def setUp(self):
    self.code1 = r"""
#include "Cad_int.h"
#include <io.h>
#include "..\env\ParamFile.h"
//#include "DontFindMe.h"

/************************************************************************************************************/

            /*
#include "orme.h"  // should work correctly!
*/

#include "..\env\linkedcontrol.h"

/* TEXT CONSTANT FOR ENUMERATION */
const WORD TxtCompType[]        = { TXT_CMP_MODL_FOURSIDED, TXT_CMP_MODL_TWOSIDED, TXT_CMP_MODL_IRREGULAR, TXT_CMP_MODL_BGA, TXT_CMP_MODL_LEADLESS, TXT_CMP_MODL_PGA, TXT_CMP_MODL_LGA, NULL };
""" 

  def test_find_cpp_comment_1(self):
    fragment = "#include f1.h\n// #include f2.h\n"
    c = CommentTool()
    self.assertEqual(c.comments(fragment), ["// #include f2.h"])

  def test_find_cpp_comment_2(self):
    fragment = "#include f1.h\n// #include f2.h"
    c = CommentTool()
    self.assertEqual(c.comments(fragment), ["// #include f2.h"])

  def test_find_c_comment_1(self):
    fragment = """/*
Yippee!
*/"""
    c = CommentTool()
    self.assertEqual(c.comments(fragment), [fragment])

  def test_find_c_comment_2(self):
    fragment = """
    Code(a,b,c);

    /*
    TESTSUITE : blah */

    return false;
    """
    c = CommentTool()
    self.assertEqual(c.comments(fragment), ["/*\n    TESTSUITE : blah */"])

  def test_find_c_comments_3(self):
    fragment = '''
    printf("/* this is not a comment */");
    /* this is a comment */'''
    c = CommentTool()
    self.assertEqual(c.comments(fragment), ["/* this is a comment */"])

  def test_find_2_comments(self):
    fragment = '''
    // test comment 1
    printf("// this is not a comment");
    printf("/* this is also no comment */");
    /* this is */
    '''
    c = CommentTool()
    self.assertEqual(c.comments(fragment), ["// test comment 1", "/* this is */"])

  def test_nested_comment(self):
    fragment = '/* how about a comment // like this\nand this\n*/'
    c = CommentTool()
    self.assertEqual(c.comments(fragment), ['/* how about a comment // like this\nand this\n*/'])

  def test_longer_fragment(self):
    c = CommentTool()
    comments = c.comments(self.code1)
    self.assertIn('//#include "DontFindMe.h"', comments)
    self.assertIn('/************************************************************************************************************/', comments)
    self.assertIn('/*\n#include "orme.h"  // should work correctly!\n*/', comments)
    self.assertIn('/* TEXT CONSTANT FOR ENUMERATION */', comments)

if __name__ == "__main__":
    unittest.main()

