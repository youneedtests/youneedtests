# This file is part of YouNeedTests.
#
#    YouNeedTests is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    YouNeedTests is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with YouNeedTests.  If not, see <http://www.gnu.org/licenses/>.

import os
import fnmatch
def find_all_files_recursively(directory, filenamepattern, excludefolders=[]):
    """ 
    Find all finds in a folder and its subfolders
        * lists all files in directory (recursively descends into underlying folders)
        * only returns filenames conforming to pattern filenamepattern (e.g. "*.txt")
        * skips directory names (even if they conform to the filenamepattern) and only shows real files
    """
    import os.path
    abs_exclude_folders = [ os.path.abspath(f) for f in excludefolders ]
    if not os.path.isdir(directory):
        #print("WARNING: skipping non-existant directory %(d)s" % { 'd' : directory })
        return []

    stack = [directory]
    files = []
    while stack:
        directory = stack.pop()
        #print ("Directory: ", directory, " Exclude: ",  abs_exclude_folders)
        if directory not in abs_exclude_folders:
          for f in os.listdir(directory):
              fullname = os.path.abspath(os.path.join(directory, f))
              if os.path.isfile(fullname) and fnmatch.fnmatch(fullname.split(os.sep)[-1],filenamepattern):
                  files.append(fullname)
              if os.path.isdir(fullname) and not os.path.islink(fullname):
                  stack.append(fullname)
    return files

def subtract_path(abs_path, longer_abs_path):
  """
  return the part of the path that only exists in longer_path
  """
  if not (longer_abs_path.startswith(abs_path)):
    return longer_abs_path
  else:
    return longer_abs_path[len(abs_path)+1:]

if __name__=='__main__':
    files = find_all_files_recursively(".", "*.cpp")
    print(files)

